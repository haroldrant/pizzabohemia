# Titulo del proyecto
#### Plantilla para páginas web

## Índice
1. [Características](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalacion)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución](#institución)

#### Características
- Uso de Css recomendado: [Ver](https://www.youtube.com/watch?v=W6GTDfrWjXs&ab_channel=Fazt)
- Uso de JavaScript recomendado: [Ver](https://www.youtube.com/watch?v=RqQ1d1qEWlE&ab_channel=Fazt)
- Uso de Html recomendado: [Ver](https://www.youtube.com/watch?v=rbuYtrNUxg4&t=6s)
#### Contenido del proyecto
- [index.html](https://gitlab.com/haroldrant/pizzabohemia/-/blob/main/index.html) : Este archivo muestra la página principal.
#### Tecnologías
- Uso de html [Puede aprender html aquí](https://www.youtube.com/watch?v=kN1XP-Bef7w&t=1s&ab_channel=SoyDalto)
- Uso de CSS [Puede aprender css aquí](https://www.youtube.com/watch?v=OWKXEJN67FE&ab_channel=SoyDalto)
- Uso de JSP [Puede aprender JSP aquí](https://www.youtube.com/watch?v=ed-OyXDxNjM&ab_channel=pildorasinformaticas)
#### IDE
- [VisualStudio Code](https://code.visualstudio.com/Download)
#### Instalación
1. local
    - Descarga el repositorio ubicado en [Descargar](https://gitlab.com/haroldrant/pizzabohemia)
    - Abrir el archivo index.html desde el navegador [Abrir](index.html)
2. gitlab
    - Realizando un fork
3. Utilización [Demo](#Demo)
#### Demo
Se puede visualizar la versión demo desplegada [aquí](https://pizzabohemia.com.co/)
#### Autor(es)
Realizado por: [Harold Rueda Antolinez](<haroldrant@ufps.edu.co>)
[Oscar Bayona](<oscarivanbdia@ufps.edu.co >)
[Santiago Alferez](<santiagofelipeavil@ufps.edu.co>)
#### Institución
Proyecto desarrollado en la materia de Proyecto social de Ingeniería de [Ingeniería de Sistemas](https://ingsistemas.cloud.ufps.edu.co/) en la [Universidad Francisco de Paula Santander](https://ww2.ufps.edu.co/)
